SE Assignment by Ninad P.Chaudhari

Current commit :
Application exceeds specifications specified by Lab Assignment 2 -- PDF provided.

TAG Assignment 1 :
Application exceeds spefications specified by Lab Assignment 1 - PDF provided

#Config :
- The app `automatically` creats table named : "nc731749"
- The environment variables should be as following
a. Server Name: ICSI518_SERVER
b. Port Number: ICSI518_PORT
c. Database Name: ICSI518_DB
d. User Name: ICSI518_USER
e. Password: ICSI518_PASSWORD

#Extra 
- The application shows list of all users.
#To Run
- Tested on Tomcat 8
- Import project in working directory of tomcat.

#To Run in your IDE
- Import project
- Select the project title.
- Hit Run, Default page has been set to Register and hence should take you there automatically.


Assignment 1 comments :

#Switch to TAG "Assignment 1"
The program has extra awesome features you will love !

- User persistancy for Application Life has been maintained.
- You have additional page to track the registered users.
- Basic login and register functions. Note, password is in plain text (as mentioned in assignment)... 
- JSF AJAX for login ! I do not need to refresh the page for checking login info. 


--Author
Ninad P.Chaudhari
ninadtech.com