
//alert();
$("#intradayGraphDivLoading").html("Loading ... Please wait");
$("#dailyGraphDivLoading").html("Loading ... Please wait");
 $("#term").hide();
var term = $("#term").val();
var term = "MSFT";
console.log(term);

var intradayString = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol="+term+"&interval=1min&apikey=XOI9L5EH2DZFQ50E&datatype=csv";
console.log(intradayString);
Plotly.d3.csv(intradayString, function(err, rows){

	  function unpack(rows, key) {
	  return rows.map(function(row) { return row[key]; });
	}

	  
	var trace1 = {
	  type: "scatter",
	  mode: "lines",
	  name: 'High',
	  x: unpack(rows, 'timestamp'),
	  y: unpack(rows, 'high'),
	  line: {color: '#17BECF'}
	}

	var trace2 = {
	  type: "scatter",
	  mode: "lines",
	  name: 'Low',
	  x: unpack(rows, 'timestamp'),
	  y: unpack(rows, 'low'),
	  line: {color: '#7F7F7F'}
	}
	var trace3 = {
			  type: "scatter",
			  mode: "lines",
			  name: 'Closing',
			  x: unpack(rows, 'timestamp'),
			  y: unpack(rows, 'close'),
			  line: {color: '#7F7F7F'}
			}

	var data = [trace1,trace2,trace3];
	    
	var layout = {
	  title: 'Intraday Prices', 
	};

	Plotly.newPlot('intradayGraphDiv', data, layout);
	$("#intradayGraphDivLoading").html("");
	});

var dailyString = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol="+term+"&apikey=XOI9L5EH2DZFQ50E&datatype=csv";
Plotly.d3.csv(dailyString, function(err, rows){

	  function unpack(rows, key) {
	  return rows.map(function(row) { return row[key]; });
	}

	  
	var trace1 = {
	  type: "scatter",
	  mode: "lines",
	  name: 'High',
	  x: unpack(rows, 'timestamp'),
	  y: unpack(rows, 'high'),
	  line: {color: '#17BECF'}
	}

	var trace2 = {
	  type: "scatter",
	  mode: "lines",
	  name: 'Low',
	  x: unpack(rows, 'timestamp'),
	  y: unpack(rows, 'low'),
	  line: {color: '#7F7F7F'}
	}
	var trace3 = {
			  type: "scatter",
			  mode: "lines",
			  name: 'Closing',
			  x: unpack(rows, 'timestamp'),
			  y: unpack(rows, 'close'),
			  line: {color: '#7F7F7F'}
			}

	var data = [trace1,trace2,trace3];
	    
	var layout = {
	  title: 'Daily Prices', 
	};

	Plotly.newPlot('dailyGraphDiv', data, layout);
	$("#dailyGraphDivLoading").html("");
	});