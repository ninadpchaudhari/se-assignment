package Configs;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbSingleton {
	String server = System.getenv("ICSI518_SERVER");
	String port = System.getenv("ICSI518_PORT");
	String DB = System.getenv("ICSI518_DB");
	String db_user = System.getenv("ICSI518_USER");
	String db_password = System.getenv("ICSI518_PASSWORD");
	String baseTable = "nc731749";
	
	Connection con = null;
	private static DbSingleton instance =null;
	private DbSingleton() {
		
	}
	
	public static DbSingleton getInstance() {
		if(instance == null) {
			instance = new DbSingleton();
			
		}
		return instance;
	}
	public Connection getConn() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection( "jdbc:mysql://"+this.server+":"+this.port+"/"+this.DB,this.db_user,this.db_password);
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	public void closeConn() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getBaseTable() {
		return this.baseTable + "_";
	}
}
