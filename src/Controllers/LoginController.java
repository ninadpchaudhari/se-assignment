package Controllers;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import Models.User;
import Daos.UserDao;


/**
 * Controller to hadle Login Page
 * @author ninad
 *
 */

@ManagedBean(name="LoginController", eager=true)
@RequestScoped
public class LoginController {
	//Variables for storing the values during login
	private String username;
	private String password;
	private String loginStatus;


	
	/*
	 * The function uses the values set in DB
	 * To be called after username and password are set in the bean.
	 */
	public String tryLoginFromDB() {
		
		System.out.println("Called DB Login");
		//Check if the username exists in the Map
		try {
			UserDao db = new UserDao();
			Models.User myUser = db.loginUser(this.username, this.password);
			if(myUser!= null) {
				// => User with secified username exists in our record
				
					this.loginStatus = "Sucessfully logged in "+ myUser.getFirstName();
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("loginStatus",loginStatus);
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("login_user",myUser);
					return "userDashboard?faces-redirect=true";
			}
			//If the function did not return yet, user does not exist or password is wrong
			this.loginStatus = "User with specified username & password Not found";
			//System.out.println(this.loginStatus + "User:" +this.username + "Passw: " +this.password );

			
		}catch(Exception e) {
			System.out.println("Failed to try Login from DB some error occured");
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * Logic to logout the user
	 */
	public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login?faces-redirect=true";
    }
	
	//Getters and Setters
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}


	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the loginStatus
	 */
	public String getLoginStatus() {
		return loginStatus;
	}

	public String redirectToDash() {
		return "userDashboard?faces-redirect=true";
	}

}
