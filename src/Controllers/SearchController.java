package Controllers;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.flow.FlowScoped;

import Models.User;
import Services.FakeStocksService;
import Services.StocksService;

@ManagedBean(name="SearchController")
@SessionScoped
public class SearchController {

	String term;
	String currentPrice;
	String quantity;
	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}
	
	
	
	
	public String getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String loadStocks() {
		
		StocksService sc = new StocksService();
		//FakeStocksService sc = new FakeStocksService();
		//sc.refreshPrices();
		this.currentPrice = sc.getCurrentPrice(this.term);
		System.out.println(sc.hello());
		return "showStock";
	}
	
	
	


	
}
