package Controllers;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import Daos.UserDao;
import Models.StockList;
import Models.User;
import Services.StocksService;
import Services.UserService;

@ManagedBean(name="TxManager")
@RequestScoped
public class TxManager {
	String term;
	String currentPrice;
	String quantity;
	String result = new String("default");
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(String currentPrice) {
		this.currentPrice = currentPrice;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	
	public String buyStock() {
		System.out.println("In buyStock");
		User currentUser = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().getOrDefault("login_user", null);
		if(this.currentPrice == null ) System.out.println("currentprince null");
		if(this.quantity== null ) System.out.println("quantity null");
		if(this.term== null ) System.out.println("term is null");
		System.out.println(this.currentPrice+this.term+this.quantity);
		if(currentUser != null) {
			System.out.println("User not null");
			double moneyToReduce = Integer.parseInt(this.quantity) * Double.parseDouble(this.currentPrice);
			double currentBalance = Float.parseFloat(currentUser.getBalance()) ;
			if(currentBalance >= moneyToReduce) {
				//proceed with the tx
				double newBalance = currentBalance - moneyToReduce;
				currentUser.setBalance(String.valueOf(newBalance));
				// Update user
				UserService us = new UserService();
				boolean userUpdate = us.updateUser(currentUser);
				// Save the stocks to user;s profile
				StocksService sc = new StocksService();
				boolean stockSave = sc.saveTx(currentUser.getId().toString(), this.term, this.currentPrice, this.quantity);
				if(userUpdate && stockSave)
				this.result = "Purchase of " + this.term + " Sucessful ! Please buy some more";
				else 
					this.result = "This is awkward!! Seems something went wrong !";
			}
			else {
				this.result = "Cannot Purchase ! - Account balance too low";
			}
			
			
			return "BuyStock";
		}
		System.out.println("User is null");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("registerSuccess", "Please Login and Try again");
		return "login?faces-redirect=true";
	}
	
	public ArrayList<StockList> getAllTx(){
		StocksService sc = new StocksService();
		ArrayList<StockList> sl = sc.getAllStockTx("1");
		return sl;
	}

}
