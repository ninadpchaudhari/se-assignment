package Daos;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import Configs.DbSingleton;
import Models.StockList;
import Models.User;

import java.sql.*;
import java.util.ArrayList;

public class StocksDao {

	String db_tableName =null;
	String db_userTableName = null;
	
	public StocksDao() throws SQLException {
		
		this.db_tableName = DbSingleton.getInstance().getBaseTable()+ "stocktx";
		this.db_userTableName = DbSingleton.getInstance().getBaseTable()+ "users";
		if(!this.createTable()) throw new SQLException("Failed to create the table///");
	}
	
	public boolean createTable() {
		System.out.println("Creating Stocktx Table in DB");
		try {
	
			//Create users table if it doesnt not exist 
			String tableStmt = "CREATE TABLE IF NOT EXISTS `"+ this.db_tableName+"` (\r\n" + 
					"  `id` INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"  `user_id` INT NOT NULL,\r\n" + 
					"  `stock` VARCHAR(45) NOT NULL,\r\n" + 
					"  `rate` VARCHAR(45) NOT NULL,\r\n" + 
					"  `quantity` VARCHAR(45) NOT NULL,\r\n" + 
					"  `timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,\r\n" + 
					"  PRIMARY KEY (`id`),\r\n" + 
					"  FOREIGN KEY (`user_id`) REFERENCES "+ this.db_userTableName +"(id) );\r\n" + 
					"";
			//System.out.println(tableStmt);
			Statement createTableStatement = DbSingleton.getInstance().getConn().createStatement();
			
			createTableStatement.executeUpdate(tableStmt);
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn for Table creation");
				DbSingleton.getInstance().getConn().close();
				System.out.println("Table creation success");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return true;
		
	}
	
	
	/*
	 * This method uses JDBC to save data to the MySQl DB
	 * @return true => Sucess
	 * @return false => Failure in insertion
	 */
	public boolean addTx(String user_id,String stockTerm,String rate, String quantity) {
		System.out.println("Trying to insert tx for use_id: "+ user_id);
		int result = 0; 
		
		
		try{
			

			
			//Statement for insertion
			PreparedStatement stmt = DbSingleton.getInstance().getConn()
					.prepareStatement("insert into "+this.db_tableName+"(user_id,stock,rate,quantity)"
					+ " values(?,?,?,?)"); 
			//Setting variables in statement
			stmt.setString(1, user_id);  
			stmt.setString(2, stockTerm); 
			stmt.setString(3, rate); 
			stmt.setString(4, quantity); 
	
			//Executing the ststement 
			
			result = stmt.executeUpdate();  
		}catch(MySQLIntegrityConstraintViolationException e) {
			System.out.println("Error, User and stock exists this is awkward!!!");
		}
		catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn for insert tx");
				DbSingleton.getInstance().getConn().close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(result == 1){  
			System.out.println("Tx inserted success");
			return true;  
		}else {
			System.out.println("Tx inserted Failed");
			return false;
		}

	}
	
	
	public ArrayList<StockList> getAllTx(String user_id){
ArrayList<StockList> allStocksArray = new ArrayList<StockList>();
		
		ResultSet rs = null;
		StockList stock = null;
		try{  

		
			//Statement for Selection
			PreparedStatement stmt = DbSingleton.getInstance().getConn().prepareStatement(
					"SELECT * from " +this.db_tableName + " WHERE user_id=?");
			
			stmt.setString(1, user_id);
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				do {
					stock = new StockList(rs.getString("term"),
							rs.getString("quantity"),
							rs.getString("rate"),
							rs.getString("timestamp"));
					allStocksArray.add(stock);
				}while(rs.next());
				
			}else {
				allStocksArray = null;
			}
			
		}catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn");
				DbSingleton.getInstance().getConn().close();
				System.out.println("Got all Stocktx");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return allStocksArray;
	}
	
}
