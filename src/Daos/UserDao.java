package Daos;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import Configs.DbSingleton;
import Models.User;

import java.sql.*;
import java.util.ArrayList;

public class UserDao {

	String db_tableName =null;

	
	public UserDao() throws SQLException {
		
		this.db_tableName = DbSingleton.getInstance().getBaseTable()+ "users";
		if(!this.createTable()) throw new SQLException("Failed to create the table///");
	}
	
	public boolean createTable() {
		System.out.println("Creating Table in DB");
		try {
	
			//Create users table if it doesnt not exist 
			String tableStmt = "CREATE TABLE IF NOT EXISTS `"+ this.db_tableName+"` (\r\n" + 
					"  `id` INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"  `username` VARCHAR(45) NOT NULL,\r\n" + 
					"  `password` VARCHAR(45) NOT NULL,\r\n" + 
					"  `firstName` VARCHAR(45) NOT NULL,\r\n" + 
					"  `lastName` VARCHAR(45) NOT NULL,\r\n" + 
					"  `email` VARCHAR(45) NOT NULL,\r\n" + 
					"  `phoneNumber` VARCHAR(45) NOT NULL,\r\n" + 
					"  `address` VARCHAR(255) NOT NULL,\r\n" + 
					"  `balance` VARCHAR(20) NOT NULL,\r\n" + 
					"  `status` VARCHAR(20) NOT NULL,\r\n" + 
					"  `role` VARCHAR(20) NOT NULL,\r\n" + 
					"  PRIMARY KEY (`id`),\r\n" + 
					"  UNIQUE INDEX `username_UNIQUE` (`username` ASC));\r\n" + 
					"";
			
			Statement createTableStatement = DbSingleton.getInstance().getConn().createStatement();
			
			createTableStatement.executeUpdate(tableStmt);
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn for Table creation");
				DbSingleton.getInstance().getConn().close();
				System.out.println("Table creation success");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return true;
		
	}
	
	
	/*
	 * This method uses JDBC to save data to the MySQl DB
	 * @return true => Sucess
	 * @return false => Failure in insertion
	 */
	public boolean insertInDatabase(User newUser) {
		System.out.println("Trying to insert user: "+newUser.getUsername());
		int result = 0; 
		
		
		try{
			

			
			//Statement for insertion
			PreparedStatement stmt = DbSingleton.getInstance().getConn()
					.prepareStatement("insert into "+this.db_tableName+"(username,password,firstName,lastName,email,phoneNumber,address,balance,status,role)"
					+ " values(?,?,?,?,?,?,?,?,?,?)"); 
			//Setting variables in statement
			stmt.setString(1, newUser.getUsername());  
			stmt.setString(2, newUser.getPassword()); 
			stmt.setString(3, newUser.getFirstName()); 
			stmt.setString(4, newUser.getLastName()); 
			stmt.setString(5, newUser.getEmail());  
			stmt.setString(6, newUser.getPhoneNumber()); 
			stmt.setString(7, newUser.getAddress()); 
			stmt.setString(8, newUser.getBalance()); 
			stmt.setString(9, newUser.getStatus()); 
			stmt.setString(10, newUser.getRole()); 
			//Executing the ststement 
			
			result = stmt.executeUpdate();  
		}catch(MySQLIntegrityConstraintViolationException e) {
			System.out.println("Error, User exists");
		}
		catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn for insert user");
				DbSingleton.getInstance().getConn().close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(result == 1){  
			System.out.println("User inserted success");
			return true;  
		}else {
			System.out.println("User inserted Failed");
			return false;
		}

	}
	public User findUser(String user_id) {
		
		
		ResultSet rs = null;
		User foundUser = null;
		try{  
			
			 

			 
			//Statement for Selection
			PreparedStatement stmt = DbSingleton.getInstance().getConn().prepareStatement("SELECT * from " +this.db_tableName +
					" WHERE id=?");
			
			stmt.setString(1, user_id);  
			
			//Executing the Statement
			
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				foundUser = new User(Long.parseLong(rs.getString("id")),
						rs.getString("firstName"),
						rs.getString("lastName"),
						rs.getString("email"),
						rs.getString("password"),
						rs.getString("address"),
						rs.getString("phoneNumber"),
						rs.getString("username"),
						rs.getString("balance"),
						rs.getString("status"),
						rs.getString("role")
						);
					
				
			}else foundUser = null;
			
		}catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn");
				DbSingleton.getInstance().getConn().close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return foundUser;
		
	}
	
	public User loginUser(String username,String password) {
		System.out.println("Trying to login user: "+ username);
		
		ResultSet rs = null;
		User foundUser = null;
		try{  
			
			 

			 
			//Statement for Selection
			PreparedStatement stmt = DbSingleton.getInstance().getConn().prepareStatement("SELECT id,username,password,firstName,lastName,email,phoneNumber,address,balance,status,role from " +this.db_tableName +
					" WHERE username=? and password=? and status='ENABLED'");
			
			stmt.setString(1, username);  
			stmt.setString(2, password);  
			//Executing the Statement
			
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				foundUser = new User(Long.parseLong(rs.getString("id")),
						rs.getString("firstName"),
						rs.getString("lastName"),
						rs.getString("email"),
						rs.getString("password"),
						rs.getString("address"),
						rs.getString("phoneNumber"),
						rs.getString("username"),
						rs.getString("balance"),
						rs.getString("status"),
						rs.getString("role")
						);
					
				
			}else foundUser = null;
			
		}catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn");
				DbSingleton.getInstance().getConn().close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return foundUser;
		
	}
	
	/*
	 * @return ArrayList of all Users in the system
	 */
	public ArrayList<User> getAllUsersArray(){
		System.out.println("Trying to get all Users.");
		ArrayList<User> allUsersArray = new ArrayList<User>();
		
		ResultSet rs = null;
		User foundUser = null;
		try{  

		
			//Statement for Selection
			PreparedStatement stmt = DbSingleton.getInstance().getConn().prepareStatement(
					"SELECT * from " +this.db_tableName);
			
			
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				do {
					foundUser = new User(Long.parseLong(rs.getString("id")),
							rs.getString("firstName"),
							rs.getString("lastName"),
							rs.getString("email"),
							rs.getString("password"),
							rs.getString("address"),
							rs.getString("phoneNumber"),
							rs.getString("username"),
							rs.getString("balance"),
							rs.getString("status"),
							rs.getString("role")
							);
					allUsersArray.add(foundUser);
				}while(rs.next());
				
			}else {
				foundUser = null;
			}
			
		}catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn");
				DbSingleton.getInstance().getConn().close();
				System.out.println("Got all users");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return allUsersArray;
	}
	
public boolean userExists(String username) {
		
		System.out.println("Checking if " +username + "Exists in DB");
		ResultSet rs = null;
		boolean exists = true;
		try{  

			//Statement for Selection
			PreparedStatement stmt = DbSingleton.getInstance().getConn().prepareStatement("SELECT id,username,password,firstName,lastName,email,phoneNumber,address from " +this.db_tableName +
					" WHERE username=?");
			
			stmt.setString(1, username);  
		
			//Executing the Statement
			
			System.out.println(stmt.toString());
			rs = stmt.executeQuery();
			if(rs.next()) {
				System.out.println("User exists");
				exists = true;
				
			}else {
				System.out.println("User Does not exists");
				exists = false;}
			
		}catch(Exception e){  
			System.out.println("userExists : Error in finding the user...");
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("userExist: C) "
						+ "- "
						+ "The Csing the DB conn");
				DbSingleton.getInstance().getConn().close();
				System.out.println("Userexists closed");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Userexists returning : "+exists);
		return exists;
		
	}
/*
 * This method uses JDBC to save data to the MySQl DB
 * @return true => Sucess
 * @return false => Failure in insertion
 */
public boolean updateUser(User newUser) {
	System.out.println("Trying to update user: "+newUser.getUsername());
	int result = 0; 
	
	
	try{
		

		
		//Statement for insertion
		PreparedStatement stmt = DbSingleton.getInstance().getConn()
				.prepareStatement("UPDATE "+this.db_tableName+" SET firstName = ? ,lastName = ? , email = ? , password = ? , address = ? , phoneNumber = ?,username = ?,balance = ?,status = ?,role=? WHERE `id` = ? "
				);
		//Setting variables in statement
		stmt.setString(1, newUser.getFirstName());
		stmt.setString(2, newUser.getLastName());
		stmt.setString(3, newUser.getEmail());
		stmt.setString(4, newUser.getPassword());
		stmt.setString(5, newUser.getAddress());
		stmt.setString(6, newUser.getPhoneNumber());
		stmt.setString(7, newUser.getUsername());
		stmt.setString(8, newUser.getBalance());
		stmt.setString(9, newUser.getStatus());
		stmt.setString(10, newUser.getRole());
		stmt.setString(11, newUser.getId().toString());
		
		//Executing the ststement 
		
		result = stmt.executeUpdate();  
	}catch(MySQLIntegrityConstraintViolationException e) {
		System.out.println("Error, User Update Failed");
	}
	catch(Exception e){  
		e.printStackTrace(); 
	}finally {
		//Finally close the connection...
		try {
			System.out.println("Closing the DB conn for update user");
			DbSingleton.getInstance().getConn().close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	if(result == 1){  
		System.out.println("User Update success");
		return true;  
	}else {
		System.out.println("User Update Failed");
		return false;
	}

}
}
