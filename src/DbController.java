import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

public class DbController {
	String server = System.getenv("ICSI518_SERVER");
	String port = System.getenv("ICSI518_PORT");
	String DB = System.getenv("ICSI518_DB");
	String db_user = System.getenv("ICSI518_USER");
	String db_password = System.getenv("ICSI518_PASSWORD");
	String db_tableName = "nc731749";
	
	@Resource(name="jdbc/primaryDb")
	private DataSource ds;
	
	Connection con = null;
	
	public DbController() throws SQLException {
		try {
	        Context ctx = new InitialContext();
	        ds = (DataSource)ctx.lookup("java:comp/env/jdbc/primaryDb");
	    } catch (NamingException e) {
	        e.printStackTrace();
	    } 
		if(!this.createTable()) throw new SQLException("Failed to create the table///");
	}
	
	public boolean createTable() {
		
		System.out.println("Creating Table in DB");
		try {
			Class.forName("com.mysql.jdbc.Driver");   
			if(ds == null) System.out.println("SHIT");
			 con = ds.getConnection();
			 
			 
			//Create users table if it doesnt not exist 
			String tableStmt = "CREATE TABLE IF NOT EXISTS `"+this.DB+"`.`"+this.db_tableName+"` (\r\n" + 
					"  `id` INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"  `username` VARCHAR(45) NOT NULL,\r\n" + 
					"  `password` VARCHAR(45) NOT NULL,\r\n" + 
					"  `firstName` VARCHAR(45) NOT NULL,\r\n" + 
					"  `lastName` VARCHAR(45) NOT NULL,\r\n" + 
					"  `email` VARCHAR(45) NOT NULL,\r\n" + 
					"  `phoneNumber` VARCHAR(45) NOT NULL,\r\n" + 
					"  `address` VARCHAR(255) NOT NULL,\r\n" + 
					"  PRIMARY KEY (`id`),\r\n" + 
					"  UNIQUE INDEX `username_UNIQUE` (`username` ASC));\r\n" + 
					"";
			
			Statement createTableStatement = con.createStatement();
			
			createTableStatement.executeUpdate(tableStmt);
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn for Table creation");
				con.close();
				System.out.println("Table creation success");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return true;
		
	}
	
	
	/*
	 * This method uses JDBC to save data to the MySQl DB
	 * @return true => Sucess
	 * @return false => Failure in insertion
	 */
	public boolean insertInDatabase(User newUser) {
		System.out.println("Trying to insert user: "+newUser.getUsername());
		int result = 0; 
		
		
		try{
			Class.forName("com.mysql.jdbc.Driver");     
			 con = ds.getConnection( );

			//Statement for insertion
			PreparedStatement stmt = con.prepareStatement("insert into "+this.db_tableName+"(username,password,firstName,lastName,email,phoneNumber,address)"
					+ " values(?,?,?,?,?,?,?)"); 
			//Setting variables in statement
			stmt.setString(1, newUser.getUsername());  
			stmt.setString(2, newUser.getPassword()); 
			stmt.setString(3, newUser.getFirstName()); 
			stmt.setString(4, newUser.getLastName()); 
			stmt.setString(5, newUser.getEmail());  
			stmt.setString(6, newUser.getPhoneNumber()); 
			stmt.setString(7, newUser.getAddress()); 
			//Executing the ststement 
			
			result = stmt.executeUpdate();  
		}catch(MySQLIntegrityConstraintViolationException e) {
			System.out.println("Error, User exists");
		}
		catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn for insert user");
				con.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(result == 1){  
			System.out.println("User inserted success");
			return true;  
		}else {
			System.out.println("User inserted Failed");
			return false;
		}

	}
	
	public User loginUser(String username,String password) {
		System.out.println("Trying to login user: "+ username);
		
		ResultSet rs = null;
		User foundUser = null;
		try{  
			Class.forName("com.mysql.jdbc.Driver");     
			 con = ds.getConnection();
			 
			 
			//Statement for Selection
			PreparedStatement stmt = con.prepareStatement("SELECT id,username,password,firstName,lastName,email,phoneNumber,address from " +this.db_tableName +
					" WHERE username=? and password=?");
			
			stmt.setString(1, username);  
			stmt.setString(2, password);  
			//Executing the Statement
			
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				foundUser = new User(rs.getString("username"),rs.getString("password"),
						rs.getString("firstName"),rs.getString("lastName"),
						rs.getString("email"), rs.getString("phoneNumber"),
						rs.getString("address"));
				
			}else foundUser = null;
			
		}catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn");
				con.close();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return foundUser;
		
	}
	
	/*
	 * @return ArrayList of all Users in the system
	 */
	public ArrayList<User> getAllUsersArray(){
		System.out.println("Trying to get all Users.");
		ArrayList<User> allUsersArray = new ArrayList<User>();
		
		ResultSet rs = null;
		User foundUser = null;
		try{  
			Class.forName("com.mysql.jdbc.Driver");     
			 con = ds.getConnection();
			 
			 
			//Statement for Selection
			PreparedStatement stmt = con.prepareStatement("SELECT id,username,password,firstName,lastName,email,phoneNumber,address from " +this.db_tableName);
			
			
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				do {
					foundUser = new User(rs.getString("username"),rs.getString("password"),
							rs.getString("firstName"),rs.getString("lastName"),
							rs.getString("email"), rs.getString("phoneNumber"),
							rs.getString("address"));
					allUsersArray.add(foundUser);
				}while(rs.next());
				
			}else {
				foundUser = null;
			}
			
		}catch(Exception e){  
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("Closing the DB conn");
				con.close();
				System.out.println("Got all users");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return allUsersArray;
	}
	
public boolean userExists(String username) {
		
		System.out.println("Checking if " +username + "Exists in DB");
		ResultSet rs = null;
		boolean exists = true;
		try{  
			Class.forName("com.mysql.jdbc.Driver");     
			 con = ds.getConnection();
			 
			 
			//Statement for Selection
			PreparedStatement stmt = con.prepareStatement("SELECT id,username,password,firstName,lastName,email,phoneNumber,address from " +this.db_tableName +
					" WHERE username=?");
			
			stmt.setString(1, username);  
		
			//Executing the Statement
			
			System.out.println(stmt.toString());
			rs = stmt.executeQuery();
			if(rs.next()) {
				System.out.println("User exists");
				exists = true;
				
			}else {
				System.out.println("User Does not exists");
				exists = false;}
			
		}catch(Exception e){  
			System.out.println("userExists : Error in finding the user...");
			e.printStackTrace(); 
		}finally {
			
			//Finally close the connection...
			try {
				System.out.println("userExist: Closing the DB conn");
				con.close();
				System.out.println("Userexists closed");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Userexists returning : "+exists);
		return exists;
		
	}
}
