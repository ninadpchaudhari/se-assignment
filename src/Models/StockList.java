package Models;

public class StockList{
	private String term;
	private String quantity;
	private String rate;
	private String timestamp;
	
	
	public StockList(String term, String quantity, String rate, String timestamp) {
		super();
		this.term = term;
		this.quantity = quantity;
		this.rate = rate;
		this.timestamp = timestamp;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}