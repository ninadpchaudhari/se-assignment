package Models;
/**
 * User Model
 * @author ninad
 *
 */
public class User {
		
		private Long id;
		private String firstName;
		private String lastName;
		private String email;
		protected String password;
		private String address;
		private String phoneNumber;
		protected String username;
		private String balance;
		private String status; // ENABLED DISABLED
		private String role; // ADMIN,CUSTOMER,MANAGER
		
		
		public User(Long id, String firstName, String lastName, String email, String password, String address,
				String phoneNumber, String username, String balance, String status, String role) {
			super();
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.password = password;
			this.address = address;
			this.phoneNumber = phoneNumber;
			this.username = username;
			this.balance = balance;
			this.status = status;
			this.role = role;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			//TODO : Hash and salt before setting
			this.password = password;
		}
		/**
		 * @return the address
		 */
		public String getAddress() {
			return address;
		}
		/**
		 * @param address the address to set
		 */
		public void setAddress(String address) {
			this.address = address;
		}
		/**
		 * @return the phoneNumber
		 */
		public String getPhoneNumber() {
			return phoneNumber;
		}
		/**
		 * @param phoneNumber the phoneNumber to set
		 */
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		/**
		 * @return the userName
		 */
		public String getUsername() {
			return username;
		}
		/**
		 * @param userName the userName to set
		 */
		public void setUsername(String username) {
			this.username = username;
		}

		public String getFullName() {
			return this.getFirstName()+ " " + this.getLastName();
		}

		public String getBalance() {
			return balance;
		}

		public void setBalance(String balance) {
			this.balance = balance;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		@Override
		public String toString() {
			return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
					+ ", password=" + password + ", address=" + address + ", phoneNumber=" + phoneNumber + ", username="
					+ username + ", balance=" + balance + ", status=" + status + ", role=" + role + "]";
		}


		

		
		
}
