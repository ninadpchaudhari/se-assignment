package Services;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import Daos.StocksDao;
import Models.StockList;

public class StocksService {
	private Client client;
	private WebTarget target;
	private StocksDao myDao;
	public StocksService() {
		try {
			this.myDao = new StocksDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean refreshPrices() {
		String symbl = new String ("AAPL");
		Client client= ClientBuilder.newClient();
		WebTarget target= client.target("https://www.alphavantage.co/query")
				   .queryParam("function", "TIME_SERIES_INTRADAY")
				   .queryParam("symbol", symbl)
				   .queryParam("interval", "1min")
				   .queryParam("apikey", "XOI9L5EH2DZFQ50E");
		String data = target.request(MediaType.APPLICATION_JSON).get(String.class);
		
		try {
			// Use Jackson to read the JSON into a tree like structure
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(data);
			
			// Make sure the JSON is an object, as said in their 	documents
			assert root.isObject();
			
			// Read "Weekly Time Series" property of root JSON object
			Iterator<String> dates = root.get("Time Series (1min)").fieldNames();
			while(dates.hasNext()) {
				// Read the first date's open price
				String time = dates.next();
				String n = root.at("/Time Series (1min)/" + time  + "/4. close").asText();
				System.out.println(time);
				System.out.println(Double.parseDouble(n));
				// remove break if you wan't to print all the open prices.
				break;
			}
		} catch (JsonParseException j) {
			System.err.println("Failed to parse Json. Data received wasn't in JSON Format");
			j.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return true;
	}
	
	public String getCurrentPrice(String symbl) {
		
		Client client= ClientBuilder.newClient();
		WebTarget target= client.target("https://www.alphavantage.co/query")
				   .queryParam("function", "TIME_SERIES_INTRADAY")
				   .queryParam("symbol", symbl)
				   .queryParam("interval", "1min")
				   .queryParam("apikey", "XOI9L5EH2DZFQ50E");
		String data = target.request(MediaType.APPLICATION_JSON).get(String.class);
		
		try {
			// Use Jackson to read the JSON into a tree like structure
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(data);
			
			// Make sure the JSON is an object, as said in their 	documents
			assert root.isObject();
			
			// Read "Weekly Time Series" property of root JSON object
			Iterator<String> dates = root.get("Time Series (1min)").fieldNames();
			while(dates.hasNext()) {
				// Read the first date's open price
				String time = dates.next();
				String n = root.at("/Time Series (1min)/" + time  + "/4. close").asText();
				return n;
				//System.out.println(time);
				//System.out.println(Double.parseDouble(n));
				// remove break if you wan't to print all the open prices.
				//break;
			}
		} catch (JsonParseException j) {
			System.err.println("Failed to parse Json. Data received wasn't in JSON Format");
			j.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return "Error in Stock price";
	}
	public String hello() {
		return "hello ";
	}
	
	public boolean saveTx(String user_id,String stockTerm, String rate, String quantity) {
		return myDao.addTx(user_id, stockTerm, rate, quantity);
	}
	
public ArrayList<StockList> getAllStockTx(String user_id){
		return this.myDao.getAllTx(user_id);
	}
}
