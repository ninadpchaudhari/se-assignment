package Services;


import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import Models.StockList;
import Models.User;
import Daos.UserDao;

/**
 * TODO : Rename to "UserRepository"
 * Has all functions required to register and also
 * Stores the Map of all users statically hence is application scoped.
 * @author ninad
 *
 */

@ManagedBean(name="myUser")
@RequestScoped
public class UserService {
	//private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String address;
	private String phoneNumber;
	private String username;
	private String type;
	private String url;
	/*
	 * @return ArrayList of all Users in the system
	 */
	public ArrayList<User> getAllUsersArray(){
		try {
			UserDao db = new UserDao();
			return db.getAllUsersArray();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error in getting all users.... ");
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	
	/*
	 * Inserts the Current user in myUser into the static session variable
	 * And in Database
	 * @return Redirect to the show Users page.
	 */
	public String insertUser() {
		User newUser = new User(Long.parseLong("1"),this.firstName,this.lastName,this.email,this.password,this.address,this.phoneNumber,this.username,
				"100000","ENABLED","USER");
		
		
		UserDao db;
		try {
			db = new UserDao();
			if(db.insertInDatabase(newUser)) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("registerSuccess", "Successfully registered," + newUser.getFullName());
				return "login?faces-redirect=true";
			}
		}catch(MySQLIntegrityConstraintViolationException e) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Cannot insert the entry, User with username exists");
			return "error";
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "error";
	}
	public String registerManager() {
		User newUser = new User(Long.parseLong("5"),this.firstName,this.lastName,this.email,this.password,this.address,this.phoneNumber,this.username,
				"0","DISABLED","MANAGER");
		
		
		UserDao db;
		try {
			db = new UserDao();
			if(db.insertInDatabase(newUser)) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash()
				.put("registerSuccess", "Successfully registered,"
				+ newUser.getFullName()
				+ "| Please wait for the admin to approve."
						);
				return "login?faces-redirect=true";
			}
		}catch(MySQLIntegrityConstraintViolationException e) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Cannot insert the entry, User with username exists");
			return "error";
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "error";
	}
	public String registerEntity() {
		System.out.println(this.type);
		if(this.type.equals("MANAGER")) return registerManager();
		else return insertUser();
	}
	public boolean updateUser(User newUser) {
		try {
			UserDao ud = new UserDao();
			return ud.updateUser(newUser);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public String addAdminUser() {
		
		User newUser = new User(Long.parseLong("1"),"Admin-First","Admin-Last","admin@albany.edu","Admin","Admin's Long address"
				,"5183626753","Admin","0","ENABLED","ADMIN"
				);
		UserDao db;
		
			try {
				db = new UserDao();
				if(!db.userExists("Admin")) {
					if(db.insertInDatabase(newUser)) return "login?faces-redirect=true";
				}
				else return "login?faces-redirect=true";
			}
			
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
		
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("error", "Admin Creation caused some error...");
			return "error";
		
		
	}
	
	
	
	


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String approveManager(String user_id) {
		try {
			UserDao db = new UserDao();
			User currentUser = db.findUser(user_id);
			currentUser.setStatus("ENABLED");
			db.updateUser(currentUser);
			return "showUsers.xhtml?faces-redirect=true";
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "error";
		
	}
	
}
