package Validators;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import Daos.UserDao;

/**
 * Custom Validator for username
 * Checks if the username is already there.
 * The FacesValidator does not support DI... Its kinda weird. This works
 * Just for reference, visit https://goo.gl/uZqh7n
 * @author ninad
 *
 */
@ManagedBean(name="UsernameValidator")
@RequestScoped
public class UsernameValidator implements Validator{

		@Override
		public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
			// TODO Auto-generated method stub
			String username = (String) arg2;
			UserDao db;
			try {
				db = new UserDao();
				if(db.userExists(username)) {
					//Username exists
					//Create the FacesMessage and throw error
					FacesMessage msg = new FacesMessage("Username exists");
				    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				    throw new ValidatorException(msg);
					
				}
				System.out.println("User does not exist, not raising any exceptions");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			//If it does not exist, then do nothing... 
		}
}
